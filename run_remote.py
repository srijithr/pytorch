import sys
import paramiko
import argparse
#import pysftp

ssh_client=paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
hostname='cube.sv.vt.edu'
username='srijith'
sshkey='/Users/srijithraj/Downloads/srijithdatanalytics.pem'


def print_totals(transferred, toBeTransferred):

    if( toBeTransferred > 1000000000 & ((transferred % 1000000) == 0)):
        print("\r Transferred: {0}GB\tOut of: {1}GB".format(transferred/1024**3, toBeTransferred/1024**3))
    elif( toBeTransferred > 1000000 &  ((transferred % 10000) == 0) ):
        #print("\r Transferred: {0}MB\tOut of: {1}MB".format(transferred/1024**2, toBeTransferred/1024**2))
        sys.stdout.write("\r Transferred: {0}MB\tOut of: {1}MB".format(transferred/1024**2, toBeTransferred/1024**2))

def run_commands(command):
    stdin,stdout,stderr=ssh_client.exec_command(command,get_pty=True)
    readlines = stdout.readlines()
    for i in readlines:
        print(i)
    readlines = stderr.readlines()
    for i in readlines:
        print(i)

def push_file(local_file_name,remote_file_name):
    ftp_client=ssh_client.open_sftp()
    ftp_client.put(local_file_name,remote_file_name)
    ftp_client.close()

if __name__ ==  "__main__":
    parser = argparse.ArgumentParser(description='Remote Runner')
    parser.add_argument('--file', type=str, default='None',
                    help='Filename to run remotely')
    parser.add_argument('--remote', type=str, default='cube.sv.vt.edu',
                    help='Remote machine used to run the program')
    args = parser.parse_args()
    filename = args.file
    if(args.remote == 'None'):
        print("-----------------------------------------------")
        print("No machine provided, running on Cube by default")
        print("-----------------------------------------------")
    else:
        hostname=args.remote
        print("-----------------------------------------------")
        print("Connecting to remote ",hostname)
        print("-----------------------------------------------")

    print(ssh_client.connect(hostname=hostname,username=username))

    print("Copying file ",filename)
    push_file('/Users/srijithraj/Documents/Development/pytorch/'+filename,'/home/srijith/pytorch/'+filename)
    print("Running command")
    print("-------------------------------------------------------------------------------------")
    print("cd pytorch; source activate pytorch; pwd; echo $CONDA_DEFAULT_ENV; python "+filename)
    print("-------------------------------------------------------------------------------------")
    run_commands("cd pytorch; source activate pytorch; pwd; echo $CONDA_DEFAULT_ENV; python "+filename)
