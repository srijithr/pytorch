import torch
import torch.nn as nn
import numpy as np

x_values = [i for i in range(100)]
x_train = np.array(x_values, dtype=np.float32)
x_train = x_train.reshape(-1, 1)

y_values = [2*i + 10.0 for i in x_values]
y_train = np.array(y_values, dtype=np.float32)
y_train = y_train.reshape(-1, 1)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

'''
CREATE MODEL CLASS
'''
class LinearRegressionModel(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(LinearRegressionModel, self).__init__()
        self.linear = nn.Linear(input_dim, output_dim)

    def forward(self, x):
        out = self.linear(x)
        return out

    def print_layer_parameters(self):
        for name,child in model.named_children():
                print(name,child)
                for names,params in child.named_parameters():
                    print(names,params)
                    print(params.size())


def closure():
        optimizer.zero_grad()
        output = model(input)
        loss = loss_fn(output, target)
        loss.backward()
        return loss

'''
INSTANTIATE MODEL CLASS
'''
input_dim = 1
output_dim = 1

model = LinearRegressionModel(input_dim, output_dim)

model.to(device)

'''
INSTANTIATE LOSS CLASS
'''

criterion = nn.MSELoss()


learning_rate = 0.001

LBFGS_FLAG = 1

if (LBFGS_FLAG == 0):
     optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum = 0.9)
else:
     optimizer = torch.optim.LBFGS(model.parameters())

epochs = 10

for epoch in range(epochs):

    epoch += 1
    inputs = torch.from_numpy(x_train).to(device)
    labels = torch.from_numpy(y_train).to(device)

    if(LBFGS_FLAG == 0):
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        print('epoch {}, loss {}'.format(epoch, loss.data[0]))
    else:
        def closure():
            global loss_fn
            optimizer.zero_grad()
            output = model(inputs)
            loss = criterion(output, labels)
            loss.backward()
            print('LBFGS epoch {}, loss {}'.format(epoch, loss.data[0]))
            return loss
        optimizer.step(closure)






model.print_layer_parameters()
